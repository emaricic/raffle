import { ethers } from "hardhat";
import { AngelDustRaffle__factory } from "../typechain-types";

async function main() {
  const Contract = (await ethers.getContractFactory(
    "AngelDustRaffle"
  )) as AngelDustRaffle__factory;

  const contract = await Contract.deploy(
    process.env.VRF_COORDINATOR!,
    process.env.LINK!,
    process.env.FEE!,
    process.env.KEY_HASH!
  );
  await contract.deployed();

  console.log(`deployed at: ${contract.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
