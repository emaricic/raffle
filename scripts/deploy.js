require("dotenv").config({ path: __dirname + "/.env" });

async function main() {
  const [deployer] = await ethers.getSigners();

  console.log("Deploying contracts with the account:", deployer.address);

  console.log("Account balance:", (await deployer.getBalance()).toString());

  const RaffleStore = await ethers.getContractFactory("AngelDustRaffle");
  const raffleStore = await RaffleStore.deploy(
    process.env.VRF_COORDINATOR,
    process.env.LINK,
    process.env.FEE,
    process.env.KEY_HASH
  );

  console.log("Raffle store address:", raffleStore.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
